resource "google_compute_network" "net-net1" {
  name                    = "net-yvan-peter-net1"
  auto_create_subnetworks = false
  mtu                     = 1460
}

resource "google_compute_subnetwork" "subnet-net1-subnet1" {
  name          = "subnet-yvan-peter-net1-subnet1"
  ip_cidr_range = "10.0.1.0/24"
  network       = google_compute_network.net-net1.id
}