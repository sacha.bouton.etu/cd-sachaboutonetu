terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "5.15.0"
    }
  }
}

provider "google" {
  # Configuration options
  project = "cd-yvan-peter"
  region  = "europe-west9"
  zone    = "europe-west9-a"
}