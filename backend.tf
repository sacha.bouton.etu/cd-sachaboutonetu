terraform {
  backend "gcs" {
    bucket = "terraform-state-yvan-peter"
    prefix = "terraform/state"
  }
}